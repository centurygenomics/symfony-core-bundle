MediaparkCoreBundle
========================

What's inside?
--------------

  * AbstractBaseEntityManager
  * AbstractBaseEvent
  
Installation
============
  
  Add repository to your `composer.json` file:
  
  ```
    "repositories": [
        {
            "type": "git-bitbucket",
            "url": "https://bitbucket.org/centurygenomics/symfony-core-bundle.git"
        }
    ],
  ```
  
  Run composer:
  
  ```shell
  composer require mediapark/corebundle dev-master
  ```
  
  Register the bundle with your kernel:
  
  ```
  // in AppKernel::registerBundles()
  $bundles = array(
      // ...
      new Mediapark\CoreBundle\MediaparkCoreBundle(),
      // ...
  );
  ```
  
How to use?
============
  
  1. AbstractBaseEntityManager and AbstractBaseEvent:
  
  Let's assume that we have User entity and we want to send an email after new user is created.
  
  * Step 1: Create UserCreated event class.
  
    ```
    <?php
    
    // src/AppBundle/Event/UserCreated.php
    
    namespace AppBundle\Event;
    
    use AppBundle\Entity\User;
    use Mediapark\CoreBundle\Event\AbstractBaseEvent;
    
    class UserCreated extends AbstractBaseEvent
    {
        public $user;
    
        /**
         * UserCreated constructor.
         * @param User $user
         */
        public function __construct(User $user)
        {
            $this->user = $user;
        }
    
        /**
         * @return User
         */
        public function getUser()
        {
            return $this->user;
        }
    
        /**
         * @return string
         */
        public function getEventName()
        {
            return "app.user_created";
        }
    }
    ```
    
  * Step 2: Create UserManager class.
  
    ```
    <?php
    
    // src/AppBundle/Manager/UserManager.php
    
    namespace AppBundle\Manager;
    
    use AppBundle\Entity\User;
    use AppBundle\Event\UserCreated;
    use AppBundle\Repository\UserRepository;
    use Mediapark\CoreBundle\Service\Manager\AbstractBaseEntityManager;
    
    class UserManager extends AbstractBaseEntityManager
    {
        /**
         * @var UserRepository
         */
        protected $repo;
    
        /**
         * @var array
         */
        protected $events = [self::EVENT_TYPE_CREATED => UserCreated::class];
    
        /**
         * @return string
         */
        protected function getClass()
        {
            return User::class;
        }
    }
    ```
    
  * Step 3: Register UserManager as service.
  
    ```
    // app/config/services.yml
    services:
        app.user_manager:
            class: AppBundle\Manager\UserManager
            parent: mediapark_core.abstract_base_entity_manager
    ```
  
  * Step 4: Create UserCreated event listener.
  
    ```
    <?php
    
    // src/AppBundle/EventListener/UserListener.php
    
    namespace AppBundle\EventListener\User;
    
    use AppBundle\Event\UserCreated;
    
    class UserListener
    {
        /**
         * @param UserCreated $userCreated
         */
        public function onUserCreated(UserCreated $userCreated)
        {
            $user = $userCreated->getUser();
    
            //send email logic
        }
    }
    ```  
    
  * Step 5: Register UserListener as service.
  
    ```
    // app/config/services.yml
    services:
        app.user_listener:
            class: AppBundle\EventListener\UserListener
            tags:
                - { name: kernel.event_listener, event: app.user_created, method: onUserCreated }
    ```
    
  * Step 6: Create new user.  
      
    ```
    //controller action
    
    $user = new User();
    $this->get('app.user_manager')->create($user);
    ```