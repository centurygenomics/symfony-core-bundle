<?php

namespace Mediapark\CoreBundle\Service\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Mediapark\CoreBundle\Entity\DispatchableEventEntityInterface;
use Mediapark\CoreBundle\Event\AbstractBaseEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class AbstractBaseEntityManager
 */
abstract class AbstractBaseEntityManager
{
    const EVENT_TYPE_CREATED = 'created';
    const EVENT_TYPE_UPDATED = 'updated';
    const EVENT_TYPE_DELETED = 'deleted';

    const ACTION_TYPE_PERSIST = 'persist';
    const ACTION_TYPE_REMOVE = 'remove';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var array = [
     * self::EVENT_TYPE_CREATED => CustomCreatedEventClass::class,
     * self::EVENT_TYPE_UPDATED => CustomUpdatedEventClass::class,
     * self::EVENT_TYPE_DELETED => CustomDeletedEventClass::class,
     * ];
     */
    protected $events = [];

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $repo;

    /**
     * AbstractBaseEntityManager constructor.
     *
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->setRepository();
    }

    /**
     * @param object $entity
     * @param bool   $autoFlush
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function create($entity, $autoFlush = true)
    {
        return $this->manageEntity($entity, $autoFlush, self::EVENT_TYPE_CREATED);
    }

    /**
     * @param object $entity
     * @param bool   $autoFlush
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update($entity, $autoFlush = true)
    {
        return $this->manageEntity($entity, $autoFlush, self::EVENT_TYPE_UPDATED);
    }

    /**
     * @param object $entity
     * @param bool   $autoFlush
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function delete($entity, $autoFlush = true)
    {
        return $this->manageEntity($entity, $autoFlush, self::EVENT_TYPE_DELETED, self::ACTION_TYPE_REMOVE);
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->getAllQuery()->getResult();
    }

    /**
     * @return array
     */
    public function getAllArray()
    {
        return $this->getAllQuery()->getArrayResult();
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllQuery()
    {
        return $this->getQueryBuilder()->select('e')
            ->orderBy('e.id', 'DESC')->getQuery();
    }

    /**
     *
     */
    public function flush()
    {
        $this->getEM()->flush();
    }

    /**
     * @param string|null $objectName
     */
    public function clear($objectName = null)
    {
        $this->getEM()->clear($objectName);
    }

    /**
     * @param integer $id
     *
     * @return null|object
     */
    public function getOneById($id)
    {
        return $this->repo->find($id);
    }

    /**
     * @param integer $id
     *
     * @return null|object
     */
    public function getReference($id)
    {
        return $this->getEM()->getReference($this->getClass(), $id);
    }

    /**
     *
     */
    public function reOpenEm()
    {
        if (!$this->em->isOpen()) {
            $this->em = $this->em->create(
                $this->em->getConnection(),
                $this->em->getConfiguration()
            );
            $this->setRepository();
        }
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->getEM()->getRepository($this->getClass())->createQueryBuilder('e');
    }

    /**
     * Entity Class name
     * @return string
     */
    abstract protected function getClass();

    /**
     * @param AbstractBaseEvent $event
     */
    protected function dispatchEvent(AbstractBaseEvent $event)
    {
        $this->dispatcher->dispatch($event->getEventName(), $event);
    }

    /**
     * @param object $entity
     * @param bool   $autoFlush
     */
    protected function persist($entity, $autoFlush = true)
    {
        $this->getEM()->persist($entity);
        if ($autoFlush) {
            $this->flush();
        }
    }

    /**
     * @param object $entity
     * @param bool   $autoFlush
     */
    protected function remove($entity, $autoFlush = true)
    {
        $this->getEM()->remove($entity);
        if ($autoFlush) {
            $this->flush();
        }
    }

    /**
     * @param object $entity
     * @param bool   $autoFlush
     * @param string $eventName
     * @param string $action
     * @return bool
     * @throws \Exception
     */
    private function manageEntity($entity, $autoFlush, $eventName, $action = self::ACTION_TYPE_PERSIST)
    {
        try {
            $this->{$action}($entity, $autoFlush);
            if ($autoFlush) {
                $this->dispatchAction($eventName, $entity);
            }

            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param string $action
     * @param object $entity
     *
     * @return bool
     */
    private function dispatchAction($action, $entity)
    {
        if ($entity instanceof DispatchableEventEntityInterface && $entity->getDispatchEvent()) {
            if (array_key_exists($action, $this->events)) {
                $event = $this->events[$action];
                $this->dispatchEvent(new $event($entity));

                return true;
            }
        }

        return false;
    }

    /**
     * Set repository by entity class variable
     */
    private function setRepository()
    {
        $this->repo = $this->getEM()->getRepository($this->getClass());
    }

    /**
     * @return EntityManagerInterface
     */
    private function getEM()
    {
        $this->reOpenEm();

        return $this->em;
    }
}
