<?php

/**
 *
 */
namespace Mediapark\CoreBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class AbstractBaseEvent
 */
abstract class AbstractBaseEvent extends Event
{
    /**
     * @return string
     */
    abstract public function getEventName();
}
