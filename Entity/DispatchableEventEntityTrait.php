<?php

namespace Mediapark\CoreBundle\Entity;

/**
 * Trait DispatchableEventEntityTrait
 */
trait DispatchableEventEntityTrait
{
    private $dispatchEvent = true;

    /**
     * @param bool $dispatchEvent
     *
     * @return object
     */
    public function setDispatchEvent($dispatchEvent)
    {
        $this->dispatchEvent = $dispatchEvent;

        return $this;
    }

    /**
     * @return bool
     */
    public function getDispatchEvent()
    {
        return $this->dispatchEvent;
    }
}
