<?php

namespace Mediapark\CoreBundle\Entity;

/**
 * Interface DispatchableEventEntityInterface
 */
interface DispatchableEventEntityInterface
{
    /**
     * @param bool $dispatchEvent
     *
     * @return object
     */
    public function setDispatchEvent($dispatchEvent);

    /**
     * @return bool
     */
    public function getDispatchEvent();
}
